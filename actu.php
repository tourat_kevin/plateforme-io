<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container">
    <div class="section text-center">
      <div class="space-30"></div>
      <h1>Nos actus !</h1>

      <!--  facebook imts -->
      
        <section class="jumbotron" style="height: 1050px !important;">
          <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v7.0"></script>
     
          <div class="fb-page img" 
            data-tabs="timeline,events,messages"
            data-href="https://www.facebook.com/institutsolacroup/posts/"
            data-width="1024"
            data-height="1000">
          </div>
      
        </section>

      <div class="space-50"></div>
      <div class="jumbotron">
          <!-- <div class="row">
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus1"></div>           
            <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus2"></div>
          </div> -->
        <div class="row">
          <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframecathy"></div>           
          <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus4"></div>
        </div>
        <div class="row">
          <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus5"></div>            
          <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus6"></div>
        </div>
        <div class="row">
          <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus7"></div>           
          <div class="embed-responsive embed-responsive-16by9 bottom-video" id="iframeactus8"></div>
        </div>
      </div>
      <div class="space-70"></div>                         
    </div>
  </div>
</div>
<?php include "inc/footer.php"; ?>