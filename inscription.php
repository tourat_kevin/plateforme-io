<?php 

	session_start();

	require('src/log.php');

	if(isset($_SESSION['connect'])){
		header('location: acceuil.php');
		exit();
	}


	if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password_two'])){

		require('src/connect.php');

		// VARIABLES
		$email 				= htmlspecialchars($_POST['email']);
		$password 			= htmlspecialchars($_POST['password']);
		$password_two		= htmlspecialchars($_POST['password_two']);

		// PASSWORD = PASSWORD TWO
		if($password != $password_two){

			header('location: inscription.php?error=1&message=Vos mots de passe ne sont pas identiques.');
			exit();

		}

		// ADRESSE EMAIL VALIDE
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){

			header('location: inscription.php?error=1&message=Votre adresse email est invalide.');
			exit();

		}

		// EMAIL DEJA UTILISEE
		$req = $db->prepare("SELECT count(*) as numberEmail FROM user WHERE email = ?");
		$req->execute(array($email));

		while($email_verification = $req->fetch()){

			if($email_verification['numberEmail'] != 0){

				header('location: inscription.php?error=1&message=Votre adresse email est déjà utilisée par un autre utilisateur.');
				exit();

			}

		}

		// HASH
		$secret = sha1($email).time();
		$secret = sha1($secret).time();

		// CHIFFRAGE DU MOT DE PASSE
		$password = "aq1".sha1($password."123")."25";

		// ENVOI
		$req = $db->prepare("INSERT INTO user(email, password, secret) VALUES(?,?,?)");
		$req->execute(array($email, $password, $secret));

		header('location: inscription.php?success=1');
		exit();

	}

?>
<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="space-30"></div>
          <div class="jumbotron">
            <div id="login-body">
              <h1>S'inscrire</h1>
                    
                    <?php if(isset($_GET['error'])){

                      if(isset($_GET['message'])) {

                        echo'<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';

                      }

                      } else if(isset($_GET['success'])) {

                        echo'<div class="alert success">Vous êtes désormais inscrit. <a href="acceuil.php">Connectez-vous</a>.</div>';

                    } ?>

              <form method="post" action="inscription.php">
                <input class="input_co" type="email" name="email" placeholder="Votre adresse email" required /><br>
                <input class="input_co" type="password" name="password" placeholder="Mot de passe" required /><br>
                <input class="input_co" type="password" name="password_two" placeholder="Retapez votre mot de passe" required /><br>
                <div class="space-30"></div>
                <button class="btn btn-outline-info" type="submit">S'inscrire</button>
              </form>
              <p class="grey">Déjà sur Plateforme.IO ? <a class="inscription" href="acceuil.php">Connectez-vous</a>.</p>
            </div>
          </div>
          <div class="space-30"></div>               
        </div>
      </div>
    </div>
  </div>
</div>
<?php include "inc/footer.php"; ?>