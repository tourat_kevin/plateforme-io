<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>
    
<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">                         
          <div class="space-50"></div>
          <section class="jumbotron">          
          <div class="card text-center">
            <div class="space-30"></div>
            <div class="card-header">Wordpress</div>
            <div class="card-body">
              <div class="embed-responsive embed-responsive-16by9 bottom-video img" id="iframewordpress1"></div>
              <div class="space-30"></div>  
              <a target="_blank" rel="noopener" type="button" href="TutoWordpressMorgane.pdf" class="btn btn-outline-info">En savoir plus</a>
            </div>               
          </div>
        </section>
        <div class="space-50"></div>
        <section class="jumbotron">
          <div class="card text-center">
            <div class="space-30"></div>  
            <div class="card-header">Elementor</div>
            <div class="card-body">
              <div class="embed-responsive embed-responsive-16by9 bottom-video img" id="iframeelementor1"></div>                   
            </div>                            
          </div>
        </section>
        <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <h2>Créez vos sites Web avec WordPress 2e Edition </h2>                       
                  <div class="card-body">
                    <div class="space-30"></div>                                    
                    <p>Cet ouvrage se destine à toutes personnes, débutants ou utilisateurs plus
                            avertis, souhaitant découvrir WordPress sous toutes ses facettes, et plus
                            particulièrement aux webmasters voulant insta ller, paramétrer et gérer un
                            site sous WordPress et aux webdesigner / webdéveloppeur confirmés en
                            HTML / CSS voulant créer entièrement un thème ou en modifier un existant 
                    </p>                            
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="Créez vos sites Web avec WordPress 2e Edition - Dunod.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-30"></div>     
                  </div>
                </div>
              </div>                                   
              <div class="col-sm-6">
                <div class="card"> 
                  <h2>Créer son premier thème WordPress pour mobile</h2>              
                  <div class="card-body">
                  <div class="space-50"></div>
                  <p>À travers ce livre, vous apprendrez à créer votre propre site mobile avec le
                          CMS WordPress. En outre, vous saurez prendre en considération tous les
                          principes et problématiques liés au Web mobile. Nous espérons d'ailleurs
                          que lors de la conception d'un site web, vous penserez d'abord à sa version
                          mobile.
                  </p>
                  <div class="space-50"></div>
                  <a target="_blank" rel="noopener" type="button" href="Thibault Baillet-Créer son propre thème WordPress pour mobile avec HTML 5 & CSS 3-Eyrolles (2012).pdf" class="btn btn-outline-info">Accéder au cours</a>
                  <div class="space-30"></div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <h5>Plusieurs cours en cours de redaction prochainement mis en ligne  ! </h5>
          </section>
          <div class="space-70"></div>                                                                 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end  -->
<?php include "inc/footer.php"; ?>