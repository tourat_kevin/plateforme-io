// Création d'un élément de type h1 en javascript
var monH1 = document.createElement("H1")

// Ajout de texte dans l'élement, il n'est pas encore affiché dans la page
monH1.innerHTML = "Coucou les corsaires !"

// Ajout de l'élément à la page (dans le body), il sera alors affiché
document.getElementById("monBody").appendChild(monH1)