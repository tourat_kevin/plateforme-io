<?php
session_start();
?>

<!-- début du bloc d'en-tête -->
<header>
<div id="logo">
<!-- INSÉRER ICI L'IMAGE cinefil.png DU SOUS-DOSSIER images/illustrations/ -->
<img src="images/illustrations/cinefil.png" alt="Logo représentant les lettres de CINE FIL" />
</div>

<h1>Votre fil cin&eacute;ma</h1>


<!-- début du bloc pour le menu de navigation -->
<nav>
 <!-- liste pour le menu principal -->
<ul>
<li><a href="index.php">Accueil</a></li><!-- INSERER UN LIEN VERS LA PAGE index.html
--><li><a href="nosfilms.php">Tous nos films</a></li> <!-- INSERER UN LIEN VERS LA PAGE nosfilms.html -->
<li><a href="membres.php">Espace membres</a></li> <!-- INSERER UN LIEN VERS LA PAGE membres.html -->
<li><a href="test.php">Test</a> <!-- INSERER UN LIEN VERS LA PAGE test.html -->
	 <!-- sous-liste pour un menu secondaire  -->
	 <ul>
	  <li><a href="test1.php">Test1</a></li> <!-- INSERER UN LIEN VERS LA PAGE test1.html -->
	  <li><a href="test2.php">Test2</a></li> <!-- INSERER UN LIEN VERS LA PAGE test2.html -->
	 </ul>
	</li>
</ul>
</nav>
<!-- fin du bloc pour le menu de navigation -->

<?php
$url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];

if(isset($_POST['deconnexion'])){
	
	/* Réinitialisation du tableau de session (on le vide intégralement) */
	$_SESSION = array();
	/* Destruction de la session */
	session_destroy();
	/* Destruction du tableau de session */
	unset($_SESSION);
}

if(isset($_POST['identifiant']) && isset($_POST['mdp'])){
	$id = $_POST['identifiant'];
	$mdp = $_POST['mdp'];
	
	// connexion à la base de donnée	
	try{ // try permet de "surveiller" les erreurs
		$bdd = new PDO('mysql:host=pedago.uhb.fr; dbname=Base-ben_m_3; charset=utf8', 'ben_m', 'lNeODX7iyI4IJXBH1bW5');
	}
	catch (Exception $e){ // catch permet "d'attraper" les erreurs
		die('Erreur : '.$e->getMessage()); /* die arrête le programme en affichant un message d'erreur */
	}
		
	// requête pour vérifier l'identité de l'utilisateur
	$requete = $bdd->prepare("SELECT Mprenom, Mnom FROM membre WHERE MembreID=? AND Mnom=?");
	$requete->execute(array($mdp, $id));
	
	$resultat = $requete->fetchall();
	if(count($resultat) == 1){
		$ligne = $resultat[0];
		$_SESSION['prenom'] = $ligne['Mprenom'];
		$_SESSION['nom'] = $ligne['Mnom'];
		$_SESSION['connecté'] = True;
		$_SESSION['userID'] = $mdp;
	}
	else{
		echo "Identifiants incorrects";
	}
}

if(isset($_SESSION['connecté']) && $_SESSION['connecté'] == True){
		$nom = $_SESSION['nom'];
		$prenom = $_SESSION['prenom'];
		echo "<form id='deconnect' method='post' action='".$url."'>
					<input type='hidden' name='deconnexion' value='True'/>
					<input type='submit' value='Déconnexion'/><br/>
					Connecté en tant que ".$prenom." ".$nom."
				</form>";
}
else{
	echo "<form id='connect' method='post' action='".$url."'>
			<label for='ident'>Identifiant:</label><input type='text' name='identifiant' id='ident'/><br/>
			<label for='mdp'>Mot de passe:</label><input type='text' name='mdp' id='mdp'/><br/>
			<input type='submit' value='Connexion'/>
		</form>";
}
?>
</header>
<!-- fin du bloc d'en-tête -->
