<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>


<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->

<p>Page "Test2"</p>

<?php
	if(isset($_POST['prix']) && isset($_POST['paye'])){
		$prix = $_POST['prix'];
		$paye = $_POST['paye'];
	
		/* METTRE LE PROGRAMME DE TRAITEMENT ICI */
		if($paye>=$prix){
			$arendre = $paye - $prix;
			$nb10 = 0;
			$nb5 = 0;
			$nb1 = 0;
			echo "<p>Monnaie à rendre:</p>";
			while($arendre>=10){ /* Boucle pour rendre les billets de 10 */
				echo "<img src='images/illustrations/10euros.png' width='100px'/> ";
				$nb10++;
				$arendre = $arendre -10;
			}
			echo "<br/>";
			while($arendre>=5){ /* Boucle pour rendre les billets de 5 */
				echo "<img src='images/illustrations/5euros.png' width='100px'/> ";
				$nb5++;
				$arendre = $arendre -5;
			}
			echo "<br/>";
			while($arendre>=1){ /* Boucle pour rendre les pièces de 1*/
				echo "<img src='images/illustrations/1euro.png' width='50px'/> ";
				$nb1++;
				$arendre = $arendre -1;
			}
			
			echo "<p><strong>Il faut rendre $nb10 billets de 10 €, $nb5 billets de 5 €, et $nb1 pièces de 1 €.</strong></p>";
		}
		else{
			echo "<p><strong>Somme payée insuffisante!!</strong></p>";
		}
		
	}
?>
<form method="post" action="test2.php">
	<p><label for="prix">Entrez le prix de l'article : </label><br/><input type="text" name="prix" id="prix" /></p>
	<p><label for="paye">Entrez la somme pay&eacute;e en euros :</label> <br/><input type="text" name="paye" id="paye" /></p>
	<p><input type="submit"/></p>
</form>


</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>


<?php include("footer.php"); ?>

</body>
</html>
