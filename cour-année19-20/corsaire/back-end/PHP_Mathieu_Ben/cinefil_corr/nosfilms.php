<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>

<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->

<p>Voici la liste de nos films:</p>

<?php
$genrechoisi = "";
if(isset($_POST['genre'])){
	$genrechoisi = $_POST['genre'];
}

// connexion à la base de donnée	
try{ // try permet de "surveiller" les erreurs
	$bdd = new PDO('mysql:host=pedago.uhb.fr; dbname=Base-ben_m_3; charset=utf8', 'ben_m', 'lNeODX7iyI4IJXBH1bW5');
}
catch (Exception $e){ // catch permet "d'attraper" les erreurs
	die('Erreur : '.$e->getMessage()); /* die arrête le programme en affichant un message d'erreur */
}

// requête pour extraire la liste des genres
$requete = $bdd->prepare("SELECT DISTINCT Fgenre FROM film");
$requete->execute();
?>

<form action='nosfilms.php' method='post'>
	 <label for="genre">Choisissez un genre :</label><br />
      <select name="genre" id="genre">
<?php
$resultat = $requete->fetchall();
foreach($resultat as $ligne ){ // passe sur toutes les lignes de $resultat
	$genre = $ligne['Fgenre'] ; // récupération des données
	if($genre==$genrechoisi){
		echo "<option selected value='".$genre."'>".$genre."</option>" ; // traitement des données
	}
	else{
		echo "<option value='".$genre."'>".$genre."</option>" ; // traitement des données
	}
}
?>
	</select>
	<input type='submit' value='Valider'/>
</form>

<?php

// on teste si l'utilisateur a chois un genre
if(isset($_POST['genre'])){
	$genre = $_POST['genre'];
	
	// préparation et exécution de la requête
	$requete = $bdd->prepare("SELECT FilmID, Ftitre, Fannee FROM film WHERE Fgenre='".$genre."' ORDER BY Ftitre");
	$requete->execute();

	// récupération des lignes de résultat

	$resultat = $requete->fetchall(); // récupère un tableau contenant toutes les lignes de résultat

	echo "<table class='tab_resultat'>";
	echo "<tr><th>Titre</th><th>Année</th></tr>";
	foreach($resultat as $ligne ){ // passe sur toutes les lignes de $resultat
		$titre = $ligne['Ftitre'] ; // récupération des données
		$annee = $ligne['Fannee'] ;
		$filmid = $ligne['FilmID'] ;
		echo "<tr><td>$titre</td><td>$annee</td><td>
												<form action='filminfo.php' method='post'>
													<input type='hidden' name='filmid' value='".$filmid."'/>
													<input type='submit' value='Détails'/>
												</form>
												</td></tr>" ; // traitement des données
	}
	echo "</table>";
}
?>

</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>

<?php include("footer.php"); ?>

</body>
</html>
