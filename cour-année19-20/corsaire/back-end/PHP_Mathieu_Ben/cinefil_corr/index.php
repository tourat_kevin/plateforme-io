<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Cin&eacute; fil</title>  <!-- titre à changer -->
		<link rel="stylesheet" href="css/monstyle.css"/>
	 <!-- lier ici le HTML au CSS -->
   </head
<body>

<?php include("header.php"); ?>

<div class="fen_princip"> <!-- bloc de fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<div id="top" class="contenu"> <!-- bloc de contenu dans la fenêtre principale --> <!--RELIER CETTE DIVISION AU STYLE CORRESPONDANT DANS LA FEUILLE DE STYLE -->


<p>Bienvenue sur le site "Cin&eacute; fil" !</p>
<p>Sur ce site, vous trouverez la liste des films propos&eacute;s par votre cinéclub en accédant à la page "Tous nos films".</p>
<p>Si vous êtes membre du club, vous pourrez accéder à l'"Espace membres" en vous authentifiant.</p>
<p>Bonne visite!</p>

<!-- INSERER ICI L'IMAGE tournage.jpg QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="tournage" src="images/illustrations/tournage.jpg" alt="Illustration d'un tournage sous forme d'ombres"/>
</div>


<aside> <!-- bloc de contenu latéral -->

<!-- INSERER ICI L'IMAGE logorennes2-blancpng24.png QUI SE TROUVE DANS LE DOSSIER images/illustrations/ ET LA DIMENSIONNER POUR QU'ELLE OCCUPE 100% DE SON CONTENEUR -->
<img id="logo_img" src="images/illustrations/logorennes2-blancpng24.png" alt="logo de l'université Rennes 2"/>
</aside>

<!-- 
<section id="section1">
<p>Section 1</p>
</section><!-- Commentaire pour enlever les white-space
--><!--<section id="section2">
<p>Section 2</p>
</section>
-->

</div>

<?php include("footer.php"); ?>

</body>
</html>
