<?php
include_once('config.php');

 // 1. Analyser les paramètres passés en POST (pseudo, contenu) 
if (isset($_POST['pseudo']) && isset($_POST['contenu']) 
    && strlen($_POST['pseudo']) > 0 &&  strlen($_POST['contenu']) > 0) {

    // On récupère ici les variables envoyées par le formulaire
    $pseudo = $_POST['pseudo'];
    $contenu = $_POST['contenu'];

    try {
        /* Connexion simple à la base de données via PDO */
        $dbh = connection_db();
    
        // 2. Créer une requête qui permettra d'insérer ces données
        // Le fait de préparer / exécuter la requête permet d'éviter 
        // les injections SQL
        $query = $dbh->prepare('INSERT INTO messages SET pseudo = :pseudo, contenu = :contenu, msg_date = NOW()');
        $query->execute([
            "pseudo" => $pseudo,
            "contenu" => $contenu
        ]);

        // Ferme la connexion à la DB
        $dbh = null;

        // 3. Donner un statut de succès ou d'erreur au format JSON
        echo json_encode([
            "status" => "ok",
            "message" => "jusqu'ici tout va bien"]);
        

    } catch (PDOException $e) {
        echo json_encode([
            "status" => "erreur",
            "message" => "Impossible d'utiliser la base de données. Erreur :".$e->getMessage()]);
    }
} else {
    // L'appel de la page n'est pas fait depuis le formulaire (pas de données postées)
    echo json_encode([
        "status" => "erreur",
        "message" => "Appel incorrect"]);
}
 
?>
