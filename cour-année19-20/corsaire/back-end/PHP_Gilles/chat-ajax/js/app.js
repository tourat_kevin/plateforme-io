/* Codons un chat en HTML/CSS/Javascript avec PHP et MySQL */

/** Fonction pour récupérer le JSON des messages et les afficher correctement */
function getMessages(scroll=false){
  // 1. Créer une requête AJAX pour se connecter au serveur, et notamment au fichier actions.php
  var requete = new XMLHttpRequest();
  requete.open('GET', 'lister.php');
  // 2. Fonction de réception des données, il faut qu'elle les traite (en exploitant le JSON) et il faut qu'elle affiche ces données au format HTML
  requete.onload = function() {
    var tabObjets = JSON.parse(requete.responseText);
    var objetsFormates = tabObjets.map( function(el) { 
      return `
      <div class="message">
        <span class="heure">${el.heure}</span> 
        <span class="pseudo">${el.pseudo}</span> : 
        <span class="contenu">${el.contenu}</span>
      <div>`
    })
    msg = document.getElementById('messages')
    msg.innerHTML = objetsFormates.join('');
    if (scroll) {
      msg.scrollTop = msg.scrollHeight;
    }

  }

  requete.onerror = function() {
    msg = document.getElementById('messages')
    msg.innerHTML = '<p>Le chat est actuellement indisponible</p>';
  }

  // 3. On envoie la requête
  requete.send();
}

/** Fonction pour envoyer le nouveau message au serveur et rafraichir les messages
 */

function postMessage(event){
  // 1. Enlever l'action par défaut de submit du formulaire
  event.preventDefault();

  // 2. Définit la requête AJAX pour poster le message 
  var requete = new XMLHttpRequest();
  requete.open('POST', 'poster.php');

  // 2. Fonction de réception des données, il faut qu'elle les traite (en exploitant le JSON) et il faut qu'elle affiche ces données au format HTML
  requete.onload = function() {
    // Récupère le résultat de l'envoi sur la BDD (non utilisé)
    var msgPHP = JSON.parse(requete.responseText);
    
    if (msgPHP.status === 'erreur') {
      // Il y a 1 erreur, le message n'a pas été posté
      console.log(msgPHP.message);
    } else {
      // Mise à jour de la liste des messages
      getMessages(scroll=true);
    
      // Vide le contenu et redonne le focus sur l'élément 
      const contenu = document.querySelector('#contenu');
      contenu.value = '';
      contenu.focus();
    }
  }

  requete.onerror = function() {
    msg = document.getElementById('messages')
    msg.innerHTML = '<p>Le chat est actuellement indisponible</p>';
  }

  // 2. Récupérer les données du formulaire pour les envoyer
  const pseudo = document.querySelector('#pseudo');
	const contenu = document.querySelector('#contenu');
  
  var data = new FormData();
  data.append('pseudo', pseudo.value); // récupérer dans $_POST['pseudo']
	data.append('contenu', contenu.value);

  // 3. On envoie la requête 
  requete.send(data);

}

// Affecter la fonction postMessage au bouton submit du formulaire
document.getElementById('envoi').addEventListener("click", postMessage);

// Récupère 1 première fois les messages 
getMessages(scroll=true);
