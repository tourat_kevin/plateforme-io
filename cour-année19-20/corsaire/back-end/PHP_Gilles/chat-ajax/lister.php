<?php
  include_once('config.php');

  /* Connexion simple à la base de données via PDO */
  $dbh = connection_db();

  // Requête simple pour lister les messages
  // $sth = $dbh->query("SELECT msg_date, pseudo, contenu FROM messages ORDER BY msg_date DESC");
    
  // Requête avec la date formatée pour n'avoir que l'heure pour lister les messages
  $sth = $dbh->query("SELECT DATE_FORMAT(msg_date, \"%H:%i\") as heure,".
    " pseudo, contenu FROM messages ORDER BY msg_date DESC LIMIT 25" );
    
  /* Fetch all of the values */
  $result = $sth->fetchAll();
  echo json_encode(array_reverse($result));

?>
