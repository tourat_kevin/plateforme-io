<?php

// Récupération des variables pour mettre dans la requete
if (isset($_GET['annee'])) {
    if (strlen($_GET['annee']) >0 ) {
        $annee = $_GET['annee'];
    } else {
        $annee = "1900";
    }
} else {
    $annee = "1900";
}


// Récupération des données de la base cinema
try { 
    /* Connexion à la BDD/BD/DB (database)
     * /!\ Attention à l'encodage avec le paramètre charset=utf8
     */
    $dbh = new PDO('mysql:host=localhost;dbname=cinema;charset=utf8', 'root', 'ROOT@mysql');

    // On fait une requête : film 
    // l'argument PDO::FETCH_ASSOC permet de récupérer
    // uniquement le résultat sous forme de tableau associatif 
    $query = "SELECT Ftitre, Fannee FROM film WHERE Fannee>=$annee";
    $result = $dbh->query($query, PDO::FETCH_ASSOC);
    
    // Affichage des résultats de la requête dans une <table>
    echo "<p>Requête : $query</p>\n";
    if ($result && $result->rowCount()>0) {
        echo "<table>\n";
        echo"\t<tr><th>Titre</th><th>Année</th></tr>\n";
        // Parcours des lignes de $result
        foreach($result as $row) {
            echo "\t<tr><td>".$row['Ftitre']."</td><td>".$row['Fannee']."</td></tr>\n";
        }
        echo "</table>\n";       
    } else {
        // Gestion du cas où la requête échoue
        echo "<p>→ La requête n'a pas renvoyé de résultat</p>"; 
    }
 
    // Ferme la connexion à la DB
    $dbh = null;

} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}


?>
