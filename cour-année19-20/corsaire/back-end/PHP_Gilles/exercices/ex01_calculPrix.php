<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Calcul prix TTC</title>
</head>

<body>

<h1>Exercice calcul de prix TTC</h1>
<form action="ex01_calculPrix.php" method="post">
	<p><label for="prixHT">Entrer le prix HT</label>
	<!-- Le pattern "[0-9]+\.*[0-9]*" permet de vérifier un nombre 
		avec ou sans partie décimale (*)
	 -->
	<input type="text" name="prixHT" id="prixHT" required pattern="[0-9]*\.*[0-9]+"></p>
	<p><label for="typeTVA">Type de TVA</label>
	<select name="typeTVA">
		<option value="0">19.6%</option>
		<option value="1">7%</option>
		<option value="2">5.5%</option>
		<option value="3">2.1%</option>
    </select>
	</p>
	<input type="submit" value="Calculer">
</form>


<?php
	if(isset($_POST['prixHT']) && isset($_POST['typeTVA'])){
		// Déréférencement des variables 
		$indexTVA = $_POST['typeTVA'];
		$prixHT = $_POST['prixHT'];

		// Valeurs de TVA mises dans un tableau, indexé par le menu déroulant
		$tableauTVA = array(19.6, 7, 5.5, 2.1);

		// Calcul du prix TTC
		$TVA = $tableauTVA[$indexTVA];		
		$prixTTC = (($TVA/100)*$prixHT+$prixHT);
		
		echo "<p><strong>Prix HT : $prixHT € → prix TTC : $prixTTC € (TVA à $TVA%).</strong></p>";
	}
	else 
	{
		echo "<p>Appuyer sur le bouton calculer</p>";
	}
?>

</body>
</html>
