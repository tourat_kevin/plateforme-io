<!DOCTYPE html>
<html lang="fr">
   <head>
		<meta charset="utf-8"/>
		<title>Envoyer / Récupérer des données de formulaire</title>  
</head>

<body>

<?php
	// Affichage débug - Récupération des variables GETées
	echo '<pre>$_GET vaut:';
	var_dump($_GET);
	echo '</pre>';
?>
<h1>Envoyer / Récupérer des données de formulaire</h1>
<form action="ex00_get_formulaire.php" method="get">
	<p><label for="maValeur">Entrer 1 valeur (variable maValeur)</label>
	<input type="text" name="maValeur" id="maValeur"></p>
	<input type="submit" value="Envoyer par GET">
</form>


<?php
	if(isset($_GET['maValeur'])){
        // Traitement prévu dans le programme
		$maValeur = $_GET['maValeur'];
		echo "<p><strong>Ma valeur vaut $maValeur </strong></p>";
        echo "<p>Regarder l'URL de la page : elle a changé suite à l'envoi du formulaire.
              <br/>Les arguments passés par la méthode 'get' du formulaire sont ajoutés après le nom de la page : ".($_SERVER['PHP_SELF'])."</p>";
        echo "<p>Essayer le lien suivant pour passer de nouveaux arguments par get <br/>
            <a href=\"".($_SERVER['PHP_SELF'])."?variable1=123&variable2=toto\">".($_SERVER['PHP_SELF'])."?variable1=123&variable2=toto</a></p>";

	}
	else 
	{
        // Ici la page est chargée sans que les données du
        // formulaire aient été postées
		echo "<p>Ma valeur n'est pas définie</p>";
	}
?>

<p><a href="./">Dossier Parent</a></p>
</body>
</html>
