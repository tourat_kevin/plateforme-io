
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test de requêtes PHP</title>
</head>
<body>
    <h1>Requêtes connexions PHP/MySQL</h1>
    <h2>Connexion à la base + Requête test</h2>
    <?php 
        try { 
            /* Connexion à la BDD/BD/DB (database)
             * /!\ Attention à l'encodage avec le paramètre charset=utf8
             */
            $dbh = new PDO('mysql:host=localhost;dbname=bibliotheque;charset=utf8', 'root', 'ROOT@mysql');

            // On fait une requête : film 
            // l'argument PDO::FETCH_ASSOC permet de récupérer
            // uniquement le résultat sous forme de tableau associatif 
            $query = "SELECT livres.titre, CONCAT(auteurs.prenom, ' ', auteurs.nom) as auteur FROM `auteurs`, `livres` WHERE livres.id_auteur = auteurs.id";
            $result = $dbh->query($query, PDO::FETCH_ASSOC);
            
            // Affichage des résultats de la requête dans une <table>
            echo "<p>Requête : $query</p>\n";
            if ($result) {
                // Affichage dfe la requête
                echo "<pre>";
                var_dump($result);
                echo "</pre>";
                echo "<table>\n";
                echo"\t<tr><th>Titre</th><th>Auteur</th></tr>\n";
                // Parcours des lignes de $result
                foreach($result as $row) {
                    echo "\t<tr><td>".$row['titre']."</td><td>".$row['auteur']."</td></tr>\n";
                }
                echo "</table>\n";       
            } else {
                // Gestion du cas où la requête échoue
                echo "<p>→ La requête n'a pas renvoyé de résultat</p>"; 
            }
     ?>


    <?php
            // Ferme la connexion à la DB
            $dbh = null;

        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    ?>
</body>
</html>