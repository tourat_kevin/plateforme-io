<?php
    // Création d'un utilisateur pour les premiers tests
    require_once "config.php";

    $nomUser = "testeur";
    $motDePasse = "connexion";

    $password = password_hash($motDePasse, PASSWORD_DEFAULT);
    $erreurMsg = "";

    // Connexion à la base 
    try {
        $dbh = connection_db();
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (:username, :password)";
        $requete = $dbh->prepare($sql);
        $requete->bindParam(':username', $nomUser);
        $requete->bindParam(':password', $password);
        $requete->execute();

        $dbh = null;
    } catch (Exception $e) {
        $erreurMsg = "Erreur !: " . $e->getMessage();
    } 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Création user</title>
    </head>
    <body>
        <h1>Création utilisateur <?php echo "$nomUser"; ?></h1>
        <?php 
            if (empty($erreurMsg)) {
                echo "<p>Bonjour $nomUser". 
                "<br/>Vous pouvez vous connecter au chat avec les identifiants :".
                "<br/>nom : $nomUser".
                "<br/>mot de passe : $motDePasse</p>";
            } else {
                echo "<p>$erreurMsg</p>"; 
            }
        ?>
        <p><a href="index.php">Retour à la connexion</a>
    </body>
</html>