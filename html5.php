<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="space-70"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-50"></div>
                  <h2>Exemple HTML (css)</h2>
                  <div class="card-body">
                    <div class="space-30"></div> 
                    <p>Ce dossier contient 15 cours très simple et de quelque ligne seulement.</p>
                    <p>Démontrant quelques exemples pratique sur les différentes balises utilisables et leurs fonctionnements</p>                        
                    <div class="space-30"></div>
                    <a href="cour-année19-20/corsaire/front-end/HTML/exemple-html.rar" download="exemple-html.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>                   
                  </div>
                </div>
              </div>                                 
              <div class="col-sm-6">
                <div class="card">                     
                  <div class="card-body">                            
                    <img class="img" src="image/html.png" alt="js svg">
                    <div class="space-20"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/HTML/ExemplesHTML5/C14/exemple14-2.html" class="btn btn-outline-info">Voir exemple</a>               
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-50"></div>
                  <h2>Cour d'intoduction Html </h2>                       
                  <div class="card-body">
                    <div class="space-50"></div>                                    
                    <p>Dans ce premier cours, vous y trouverez les bases du HTML 5.</p>
                    <p>Son origine, et comment débuter avec de bonnes pratiques.</p>
                                         
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">                             
                    <img class="img" src="image/html.png" alt="js svg">
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/HTML/HTML-1.pdf" class="btn btn-outline-info">Accéder au cours</a>                  
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <div class="card">
            <p class="codepen" data-height="500" data-theme-id="light" data-default-tab="result" data-user="MiisterK" data-slug-hash="RwWYraV" style="height: 1000px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="RwWYraV">
              <span>See the Pen <a href="https://codepen.io/MiisterK/pen/RwWYraV">
                      RwWYraV</a> by Kevin Tourat (<a href="https://codepen.io/MiisterK">@MiisterK</a>)
                      on <a href="https://codepen.io">CodePen</a>.
              </span>
            </p>
            <script async src="https://static.codepen.io/assets/embed/ei.js"></script>
          </div>
          <div class="space-50"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">                 
                  <h2>Exercice Html </h2>
                  <div class="card-body">
                    <h3>Page a coder</h3>                                 
                    <p>Nous allons travailler avec Visual Studio Code pour éditer les pages HTML Les extensions nécessaires pour le web sont.. (voir la suite du cours)</p>                         
                    <p>Télécharger le .RAR pour avoir les fichiers et images. Fiche technique HTML Cheatsheet pour vous aidez</p>
                    <div class="space-30"></div>
                    <a href="cour-année19-20/corsaire/front-end/HTML/cour-vans.rar" download="cour-vans.rar" class="btn btn-outline-info"><i class="material-icons">get_app</i> Télécharger</a>
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" href="cour-année19-20/corsaire/front-end/HTML/htmlcheatsheet.pdf" class="btn btn-outline-info">fiche technique</a>                     
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                  <div class="space-50"></div>
                    <img class="img" src="cour-année19-20/corsaire/front-end/HTML/page_a_coder/PageACoder/Site-Kevin.png" alt="image cour">                                                                 
                    <div class="space-50"></div>
                    <a target="_blank" rel="noopener" type="button" href="cour-année19-20/corsaire/front-end/HTML/HTML-1-Gilles.pdf" class="btn btn-outline-info">Accéder au cours</a>
                    <div class="space-20"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-70"></div>       
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end  -->
<?php include "inc/footer.php"; ?>