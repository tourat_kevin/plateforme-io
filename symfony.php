<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>

<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="space-70"></div>
          <section class="jumbotron">
            <div class="card text-center">
            <div class="space-30"></div>
              <div class="card-header">Symfony: Le framework PHP par excellence</div>
              <div class="card-body">
                <img class="img" src="image/symfony3_logo-580x221.png" alt="symfony">
                <div class="space-30"></div> 
                <a target="_blank" rel="noopener" href="cour-année19-20/corsaire/back-end/Symfony/symfony-prise-en-main/index.html" class="btn btn-outline-info">Allez a la page</a>
              </div>
            </div>
          </section>
          <div class="space-70"></div>                                                                   
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end  -->
<?php include "inc/footer.php"; ?>