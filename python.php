<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: acceuil.php');
  exit();
}
?>

<?php include "inc/header.php"; ?>
<?php include "inc/navbar.php"; ?>
    
<div class="main main-raised ">
  <div class="container background_body">
    <div class="section text-center">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="space-70"></div>
          <section class="jumbotron">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="space-50"></div>
                  <h2>Introduction à Python</h2>                       
                  <div class="card-body">
                    <div class="space-50"></div>
                    <p>Git est un logiciel de gestion de versions décentralisé. Logiciel libre créé par Linus Torvalds, auteur du noyau Linux. Complexe mais indispensable.</p>                                                              
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="python_cheatsheet.pdf" class="btn btn-outline-info">Fiche technique</a>
                    <div class="space-20"></div> 
                  </div>
                </div>
              </div>                                     
              <div class="col-sm-6">
                <div class="card">                     
                  <div class="card-body">                             
                    <img class="img" src="image/illu_apprenez-a-programmer-en-python.png" alt="js svg">                             
                    <div class="space-30"></div>
                    <a target="_blank" rel="noopener" type="button" href="PythonKids - Jason-R-Briggs.pdf" class="btn btn-outline-info">Accéder au cours</a>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="space-50"></div>
          <div class="jumbotron">
            <div class="embed-responsive embed-responsive-16by9 img" id="iframepy1"></div>
          </div>                      
          <div class="space-70"></div>        
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end  -->
<?php include "inc/footer.php"; ?>